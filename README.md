# Executar
O programa pode ser executado através do script *Trab2mc920.py*.
O script recebe como argumento uma imagem de entrada que se encontra no mesmo diretório que o arquivo, o algoritmo irá executar todos os filtros na imagem, salvar as imagens resultantes no mesmo diretório e os respectivos histogramas.
Os histogramas foram gerados para fins comparativos e entender mais como tem sido as mudanças causadas pelos filtros.
Para rodar o script podemos fazer dessa forma :

``` python
python3 Trab2mc920.py city.png
```

# output
O output será salvo na mesma pasta e com o seguinte nome :
outfile[i].png e histograma[i], que são referentes a passagem do filtro i. Apenas o filtro 10, significa a passagem da imagem pelo filtro h4 seguido pelo h3, para comparar com a imagem do filtro 9, que deveriam obter os mesmos resultados.
O nome da imagem deve vir acompanhada do tipo, caso contrário, não será possível rodar o script e realizar a passagem de todos os filtros.