#!/usr/bin/python
import sys
import numpy as np
import scipy as sp
from scipy import signal
from scipy import misc   
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from skimage import io
from PIL import Image, ImageDraw

#a imagem que foi passada como parâmetro
nome_imagem = sys.argv[1]

#criando os filtros que iremos utilizar
#filtros utilizados
h1 =     np.array([[0, 0, -1, 0, 0],
                  [0, -1, -2, -1, 0],
                  [-1, -2, 16, -2, -1],
                  [0, -1, -2, -1, 0],
                  [0, 0, -1, 0, 0]])

h2 =     np.array([[1, 4, 6, 4, 1],
                  [4, 16, 24, 16, 4],
                  [6, 24, 36, 24, 6],
                  [4, 16, 24, 16, 4],
                  [1, 4, 6, 4, 1]])

h2 = (1/256) * h2

h3 =     np.array([[-1, 0, 1],
                   [-2, 0, 2],
                   [-1, 0, 1]])

h4 =     np.array([[-1, -2, -1],
                   [0, 0, 0],
                   [1, 2, 1]])

h5 =     np.full((3,3),-1)
h5[1][1] = 8

h6 =     np.full((3,3),1)
h6 = (1/9) * h6

h7 =     np.full((3,3),-1)
h7[0][2] = 2
h7[1][1] = 2
h7[2][0] = 2

h8 =     np.full((3,3),-1)
h8[0][0] = 2
h8[1][1] = 2
h8[2][2] = 2

h9 = np.sqrt(np.square(h3) + np.square(h4))


#abre imagem
imagem = Image.open(nome_imagem)

#passa os filtros, gera cada uma das imagens e as salva na pasta destino
image1 = sp.signal.convolve2d(imagem, h1, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile1.png",image1, format="png", cmap=cm.gray)

image2 = sp.signal.convolve2d(imagem, h2, mode='full',boundary='fill', fillvalue=0)
plt.imsave("outfile2.png",image2, format="png", cmap=cm.gray)

image3 = sp.signal.convolve2d(imagem, h3, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile3.png",image3, format="png", cmap=cm.gray)

image4 = sp.signal.convolve2d(imagem, h4, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile4.png",image4, format="png", cmap=cm.gray)

image5 = sp.signal.convolve2d(imagem, h5, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile5.png",image5, format="png", cmap=cm.gray)

image6 = sp.signal.convolve2d(imagem, h6, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile6.png",image6, format="png", cmap=cm.gray)

image7 = sp.signal.convolve2d(imagem, h7, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile7.png",image7, format="png", cmap=cm.gray)

image8 = sp.signal.convolve2d(imagem, h8, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile8.png",image8, format="png", cmap=cm.gray)

image9 = sp.signal.convolve2d(imagem, h9, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile9.png",image9, format="png", cmap=cm.gray)

image10 = sp.signal.convolve2d(image4, h3, mode='same',boundary='wrap', fillvalue=0)
plt.imsave("outfile10.png",image10, format="png", cmap=cm.gray)

#gerar histograma, abre a imagem gerada, cria o histograma e limpa o pyplot
imag = io.imread(nome_imagem)
ax = plt.hist(imag.ravel(), 256,[0,256])
plt.savefig("histograma.png")
plt.clf()

imag1 = io.imread("outfile1.png")
ax = plt.hist(imag1.ravel(),256,[0,254])
plt.savefig("histograma1.png")
plt.clf()

imag2 = io.imread("outfile2.png")
ax = plt.hist(imag2.ravel(), 256,[0,254])
plt.savefig("histograma2.png")
plt.clf()   

imag3 = io.imread("outfile3.png")
ax = plt.hist(imag3.ravel(), 256,[0,254])
plt.savefig("histograma3.png")
plt.clf()

imag4 = io.imread("outfile4.png")
ax = plt.hist(imag4.ravel(), 256,[0,254])
plt.savefig("histograma4.png")
plt.clf()

imag5 = io.imread("outfile5.png")
ax = plt.hist(imag5.ravel(), 256,[0,254])
plt.savefig("histograma5.png")
plt.clf()

imag6 = io.imread("outfile6.png")
ax = plt.hist(imag6.ravel(), 256,[0,254])
plt.savefig("histograma6.png")
plt.clf()

imag7 = io.imread("outfile7.png")
ax = plt.hist(imag7.ravel(), 256,[0,254])
plt.savefig("histograma7.png")
plt.clf()

imag8 = io.imread("outfile8.png")
ax = plt.hist(imag8.ravel(), 256,[0,254])
plt.savefig("histograma8.png")
plt.clf()

imag9 = io.imread("outfile9.png")
ax = plt.hist(imag9.ravel(), 256,[0,254])
plt.savefig("histograma9.png")
plt.clf()

imag10 = io.imread("outfile10.png")
ax = plt.hist(imag10.ravel(), 256,[0,254])
plt.savefig("histograma10.png")
plt.clf()